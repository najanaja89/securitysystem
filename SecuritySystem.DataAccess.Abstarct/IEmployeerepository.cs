﻿using SecuritySystem.Services.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.DataAccess.Abstarct
{
    public interface IEmployeeRepository
    {
        void Add(Employee employee);

        void Update(Employee employee);

        ICollection<Employee> GetALL();

        Employee GetById(int id);
    }
}
