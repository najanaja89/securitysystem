﻿using SecuritySystem.Services.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.DataAccess.Abstarct
{
    public interface IPeopeRepository
    {
        void Add(Person person);

        void Update(Person person);

        ICollection<Person> GetALL();

        Person GetById(int id);

    }
}
