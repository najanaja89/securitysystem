﻿using System;
using SecuritySystem.Services.Model;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.Services.Abstract
{
    public interface IEmployeeJournalService
    {
        void RegisterEmployeeComingtime(Employee employee);
    }
}
