﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.Services.Abstract
{
    public interface IDoorService
    {
        void Open();
    }
}
