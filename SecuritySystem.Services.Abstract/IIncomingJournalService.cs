﻿using SecuritySystem.Services.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.Services.Abstract
{
    public interface IIncomingJournalService
    {
        void RegisterVisitor(Person person);
        void RegisterVisitorLeft(Person person);
    }
}
