﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.Services.Model
{
    public class Employee
    {
        public int Id { get; set; }
        public Person Person { get; set; }
        public string Position { get; set; }
        public ICollection<DateTime> VisitsJournale { get; set; }
    }
}
