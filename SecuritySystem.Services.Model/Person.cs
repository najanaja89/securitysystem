﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecuritySystem.Services.Model
{
    public class Person
    {
        public int Id { get; set; }
        public string FullName { get; set; }
        public DateTime IncomingDate { get; set; }
        public string IIN { get; set; }
    }
}
